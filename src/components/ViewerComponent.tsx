import React from 'react';
import * as THREE from 'three';

import { Axes, ClippingComponent, Grid, Viewer } from 'web-ifc-viewer';
import { IFCLoader } from '../lib/IFCLoader';

export class ViewerComponent extends React.Component {
    containerRef!: React.RefObject<HTMLDivElement>;
    viewer!: Viewer;
    grid!: Grid;
    axes!: Axes;
    clipping!: ClippingComponent;
    ifcLoader!: IFCLoader

    constructor(props: any) {
        super(props)
        this.containerRef = React.createRef<HTMLDivElement>();
    }

    work() {
        const project = this.ifcLoader.getSpatialStructure();
        console.log(project)
        const site = project.hasSpatialChildren[0];
        const building = site.hasSpatialChildren[0];
        const stories = building.hasSpatialChildren;
        const storey = stories[0]
        const id = storey.expressID;
        this.ifcLoader.highlightMaterial = new THREE.MeshPhongMaterial();
        this.ifcLoader.highlightItems([id], this.viewer.scene, new THREE.MeshPhongMaterial());
    }

    async componentDidMount() {

        const container = this.containerRef.current!

        this.viewer = new Viewer(container);
        this.viewer.ifcLoader.setWasmPath("../../");
        this.grid = new Grid(this.viewer, 100, 100);
        this.axes = new Axes(this.viewer);
        this.clipping = new ClippingComponent(this.viewer);
        const url = "./data/big.ifc";
        const ifcFile = await this.fetchIfc(url);
        this.viewer.loadIfc(ifcFile, true);
        this.viewer.loadIfc(await this.fetchIfc("./data/bat2.ifc"), true);
        this.viewer.loadIfc(await this.fetchIfc("./data/bat9.ifc"), true);
        this.ifcLoader = this.viewer.ifcLoader;
        this.work = this.work.bind(this)
        setTimeout(this.work, 1000)


    }



    async fetchIfc(url: string): Promise<File> {
        const data = await fetch(url);
        const result = await data.text();
        const ifcBlob = new Blob([result], { type: 'text/plain' });
        const ifcFile = new File([ifcBlob], "ifcFile");
        return ifcFile;
    }

    render() {
        return (
            <div ref={this.containerRef} style={{ position: 'relative', height: '100%', width: '100%' }} ></div>
        );
    }
}