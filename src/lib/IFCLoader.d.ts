import {
	BufferGeometry,
	Loader,
	LoadingManager,
	Scene,
	Material,
	MeshBasicMaterial
} from 'three';

interface SpatialStructureElement {
	expressID: number;
	hasChildren: number[];
	hasSpatialChildren: SpatialStructureElement[];
}

export class IFCLoader extends Loader {
	highlightMaterial: MeshBasicMaterial;
	manager: LoadingManager;
	mapFaceindexID: Object;
	modelID:number;
	mapIDGeometry: Object;
	constructor( manager?: LoadingManager );
	setWasmPath(path: string): void;

	getExpressId(faceIndex: number): number;
	getItemProperties(expressId: number, all: boolean, recursive: boolean): any;
	highlightItems(expressIds: number[], scene: Scene, material: Material ): void;
	setItemsVisibility(expressIds: number[], geometry: BufferGeometry, visible: boolean ): void;
	getSpatialStructure(): SpatialStructureElement;

	load( url: string, onLoad: ( geometry: BufferGeometry ) => void, onProgress?: ( event: ProgressEvent ) => void, onError?: ( event: ErrorEvent ) => void ): void;
	parse( data: ArrayBuffer ): BufferGeometry;
	getIfcItemInformation(expressID: number): object;
}