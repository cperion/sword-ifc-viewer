import './App.css';
import { ViewerComponent } from './components/ViewerComponent'

function App() {
  return (
    <div className="App">
      <ViewerComponent></ViewerComponent>
    </div>
  );
}

export default App;
